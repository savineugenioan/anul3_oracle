CLEAR SCREEN;


DELETE FROM CONTRACTE;
DELETE FROM ACHIZITII;
DELETE FROM TESTARE;
DELETE FROM FACTURI;

/*
INSERT INTO CLIENTI VALUES(Secv_Clienti.NEXTVAL,'RO29477580','BEDELECO EAST EUROPE SRL',0,'Aleea Camilla 2,A1','RO682004000000000157841Z');
INSERT INTO CLIENTI VALUES(Secv_Clienti.NEXTVAL,'RO23221221','Light Candel ART SRL',0,'Aleea Bucuresti 2,A1','RO682004000000000167841Z');
INSERT INTO CLIENTI VALUES(Secv_Clienti.NEXTVAL,'RO27438852','STEHOS ACERO SRL',0,'Aleea Halelor 2,A1','RO682004000000000157843Z');
INSERT INTO CLIENTI VALUES(Secv_Clienti.NEXTVAL,'RO15994196','PROCEMA PERLIT SRL',0,'SOS Giurgiului 2,A1','RO682004000000000157842Z');
INSERT INTO CLIENTI VALUES(Secv_Clienti.NEXTVAL,'RO18530085','A And C TRANS COMPANY INTERNATIONAL SRL',0,'Strada Regina Elena 2,A1','RO682004000500000157841Z');
INSERT INTO CLIENTI VALUES(Secv_Clienti.NEXTVAL,'RO30897146','AGGREKO SOUTH EAST EUROPE',0,'Strada A.I. Cuza 2,A1','RO682004000000500157841Z');
INSERT INTO CLIENTI VALUES(Secv_Clienti.NEXTVAL,'RO33218827','ALEEA GARDEN SRL',0,'STR Broscari 108','RO682004002000000157841Z');
INSERT INTO CLIENTI VALUES(Secv_Clienti.NEXTVAL,'16645658','UNIMASTERS LOGISTIC SRL',0,'Drum Garii Odai 1A','RO682004009000000157841Z');
INSERT INTO CLIENTI VALUES(Secv_Clienti.NEXTVAL,'29189529','FRIGOELECTRIC INSTALATII SRL',0,'Intr. Brasov 6','RO682004050000000157841Z');
INSERT INTO CLIENTI VALUES(Secv_Clienti.NEXTVAL,'6707443','ROBITAL INDUSTRIAL SUPPLIER SRL',0,'BD. Biruintei 89','RO682004003000000157841Z');
INSERT INTO CLIENTI VALUES(Secv_Clienti.NEXTVAL,'6646761','ROLLIT PRODIMPEX SRL',0,'Aleea Erou Iancu Nicolae 126','RO682004000056000157841Z');
INSERT INTO CLIENTI VALUES(Secv_Clienti.NEXTVAL,'30490052','PROFESIONAL SECURITY DIVISION SRL',0,'STR. Fortului 113M','RO682404000000000157841Z');
INSERT INTO CLIENTI VALUES(Secv_Clienti.NEXTVAL,'36710734','GYANA CREAT PROD SRL',0,'STR Liviu Rebreanu, nr. 93','RO682004005000060157841Z');
INSERT INTO CLIENTI VALUES(Secv_Clienti.NEXTVAL,'33364695','FINESTORE DISTRIBUTION SRL',0,'Str. Horia, Closca si Crisan, nr.7','RO682004006600000157841Z');

INSERT INTO DICTIONAR_SERVICII VALUES(1,1,'Development',0.19,90,85);
INSERT INTO DICTIONAR_SERVICII VALUES(2,2,'Support',0.19,50,45);
INSERT INTO DICTIONAR_SERVICII VALUES(3,3,'Implemetare',0.19,40,35); 

*/
INSERT INTO CONTRACTE VALUES(Secv_Contracte.NEXTVAL,TO_DATE(SYSDATE),200,0,720,0,TO_DATE(SYSDATE+70),TO_DATE(SYSDATE+90),0,1);

INSERT INTO FACTURI(Serie_Numar,Total_cu_Tva,Data_Scadenta,Nr_Contract,Tip_Factura) Values(TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.NEXTVAL,200,sysdate,Secv_Contracte.CURRVAL,'AVANS');
INSERT INTO FACTURI_Detalii(Cod_Detalii,Nr_Crt,Cantitate_Ore,Serie_Numar,Denumire,Pretul_Unitar) VALUES(Secv_Facturi_Detalii.NEXTVAL,1,1,TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.CURRVAL,'AVANS',200);
INSERT INTO FACTURI(Serie_Numar,Total_cu_Tva,Data_Scadenta,Nr_Contract) Values(TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.NEXTVAL,520,sysdate,Secv_Contracte.CURRVAL);
INSERT INTO FACTURI_Detalii(Cod_Detalii,Nr_Crt,Cantitate_Ore,Serie_Numar,Id_Serviciu) VALUES(Secv_Facturi_Detalii.NEXTVAL,1,8,TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.CURRVAL,1);
INSERT INTO FACTURI_Detalii(Cod_Detalii,Nr_Crt,Cantitate_Ore,Serie_Numar,Denumire,Pretul_Unitar) VALUES(Secv_Facturi_Detalii.NEXTVAL,2,1,TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.CURRVAL,'AVANS',-200);

INSERT INTO TESTARE VALUES(Secv_Testare.NEXTVAL,sysdate,Secv_Contracte.CURRVAL);
INSERT INTO TESTARE_DETALII VALUES(Secv_Testare_Detalii.NEXTVAL,'�ndepline?te func?ionalitatea primar? ?i men?ine stabilitatea','DA',1,'DA',Secv_Testare.CURRVAL);
INSERT INTO TESTARE_DETALII VALUES(Secv_Testare_Detalii.NEXTVAL,'Functioneaza corect sub un cont de utilizator','DA',1,'DA',Secv_Testare.CURRVAL);
INSERT INTO TESTARE_DETALII VALUES(Secv_Testare_Detalii.NEXTVAL,'Functioneaza corect sub un cont de utilizator de admin','DA',1,'DA',Secv_Testare.CURRVAL);
INSERT INTO TESTARE_DETALII VALUES(Secv_Testare_Detalii.NEXTVAL,'Functioneaza corect sub un cont de administrator','DA',1,'DA',Secv_Testare.CURRVAL);
INSERT INTO TESTARE_DETALII VALUES(Secv_Testare_Detalii.NEXTVAL,'Finalizeaz? o instalare minim?','DA',1,'DA',Secv_Testare.CURRVAL);

INSERT INTO CONTRACTE VALUES(Secv_Contracte.NEXTVAL,TO_DATE(SYSDATE),300,0,900,0,TO_DATE(SYSDATE+70),TO_DATE(SYSDATE+90),0,1);

INSERT INTO FACTURI(Serie_Numar,Total_cu_Tva,Data_Scadenta,Nr_Contract,Tip_Factura) Values(TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.NEXTVAL,300,sysdate,Secv_Contracte.CURRVAL,'AVANS');
INSERT INTO FACTURI_Detalii(Cod_Detalii,Nr_Crt,Cantitate_Ore,Serie_Numar,Denumire,Pretul_Unitar) VALUES(Secv_Facturi_Detalii.NEXTVAL,1,1,TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.CURRVAL,'AVANS',300);
INSERT INTO FACTURI(Serie_Numar,Total_cu_Tva,Data_Scadenta,Nr_Contract) Values(TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.NEXTVAL,600,sysdate,Secv_Contracte.CURRVAL);
INSERT INTO FACTURI_Detalii(Cod_Detalii,Nr_Crt,Cantitate_Ore,Serie_Numar,Id_Serviciu) VALUES(Secv_Facturi_Detalii.NEXTVAL,1,10,TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.CURRVAL,1);
INSERT INTO FACTURI_Detalii(Cod_Detalii,Nr_Crt,Cantitate_Ore,Serie_Numar,Denumire,Pretul_Unitar) VALUES(Secv_Facturi_Detalii.NEXTVAL,2,1,TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.CURRVAL,'AVANS',-300);

INSERT INTO TESTARE VALUES(Secv_Testare.NEXTVAL,sysdate,Secv_Contracte.CURRVAL);
INSERT INTO TESTARE_DETALII VALUES(Secv_Testare_Detalii.NEXTVAL,'�ndepline?te func?ionalitatea primar? ?i men?ine stabilitatea','DA',1,'DA',Secv_Testare.CURRVAL);
INSERT INTO TESTARE_DETALII VALUES(Secv_Testare_Detalii.NEXTVAL,'Functioneaza corect sub un cont de utilizator','DA',1,'DA',Secv_Testare.CURRVAL);
INSERT INTO TESTARE_DETALII VALUES(Secv_Testare_Detalii.NEXTVAL,'Functioneaza corect sub un cont de utilizator de admin','DA',1,'DA',Secv_Testare.CURRVAL);
INSERT INTO TESTARE_DETALII VALUES(Secv_Testare_Detalii.NEXTVAL,'Functioneaza corect sub un cont de administrator','DA',1,'DA',Secv_Testare.CURRVAL);
INSERT INTO TESTARE_DETALII VALUES(Secv_Testare_Detalii.NEXTVAL,'Finalizeaz? o instalare minim?','DA',1,'DA',Secv_Testare.CURRVAL);

INSERT INTO CONTRACTE VALUES(Secv_Contracte.NEXTVAL,TO_DATE(SYSDATE),500,0,1500,0,TO_DATE(SYSDATE+70),TO_DATE(SYSDATE+90),0,1);

INSERT INTO FACTURI(Serie_Numar,Total_cu_Tva,Data_Scadenta,Nr_Contract,Tip_Factura) Values(TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.NEXTVAL,500,sysdate,Secv_Contracte.CURRVAL,'AVANS');
INSERT INTO FACTURI_Detalii(Cod_Detalii,Nr_Crt,Cantitate_Ore,Serie_Numar,Denumire,Pretul_Unitar) VALUES(Secv_Facturi_Detalii.NEXTVAL,1,1,TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.CURRVAL,'AVANS',500);
INSERT INTO FACTURI(Serie_Numar,Total_cu_Tva,Data_Scadenta,Nr_Contract) Values(TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.NEXTVAL,1000,sysdate,Secv_Contracte.CURRVAL);
INSERT INTO FACTURI_Detalii(Cod_Detalii,Nr_Crt,Cantitate_Ore,Serie_Numar,Id_Serviciu) VALUES(Secv_Facturi_Detalii.NEXTVAL,1,30,TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.CURRVAL,2);
INSERT INTO FACTURI_Detalii(Cod_Detalii,Nr_Crt,Cantitate_Ore,Serie_Numar,Denumire,Pretul_Unitar) VALUES(Secv_Facturi_Detalii.NEXTVAL,2,1,TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.CURRVAL,'AVANS',-500);


INSERT INTO CONTRACTE VALUES(Secv_Contracte.NEXTVAL,TO_DATE(SYSDATE),500,0,1500,0,TO_DATE(SYSDATE+70),TO_DATE(SYSDATE+90),0,2);

INSERT INTO FACTURI(Serie_Numar,Total_cu_Tva,Data_Scadenta,Nr_Contract,Tip_Factura) Values(TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.NEXTVAL,500,sysdate,Secv_Contracte.CURRVAL,'AVANS');
INSERT INTO FACTURI_Detalii(Cod_Detalii,Nr_Crt,Cantitate_Ore,Serie_Numar,Denumire,Pretul_Unitar) VALUES(Secv_Facturi_Detalii.NEXTVAL,1,1,TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.CURRVAL,'AVANS',500);
INSERT INTO FACTURI(Serie_Numar,Total_cu_Tva,Data_Scadenta,Nr_Contract) Values(TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.NEXTVAL,1000,sysdate,Secv_Contracte.CURRVAL);
INSERT INTO FACTURI_Detalii(Cod_Detalii,Nr_Crt,Cantitate_Ore,Serie_Numar,Id_Serviciu) VALUES(Secv_Facturi_Detalii.NEXTVAL,1,30,TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.CURRVAL,2);
INSERT INTO FACTURI_Detalii(Cod_Detalii,Nr_Crt,Cantitate_Ore,Serie_Numar,Denumire,Pretul_Unitar) VALUES(Secv_Facturi_Detalii.NEXTVAL,2,1,TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.CURRVAL,'AVANS',-500);

INSERT INTO TESTARE VALUES(Secv_Testare.NEXTVAL,sysdate,Secv_Contracte.CURRVAL);
INSERT INTO TESTARE_DETALII VALUES(Secv_Testare_Detalii.NEXTVAL,'Porne?te de la consol?','DA',1,'DA',Secv_Testare.CURRVAL);
INSERT INTO TESTARE_DETALII VALUES(Secv_Testare_Detalii.NEXTVAL,'Porne?te de la fi?ierul autorun pe CD-ul aplica?iei','DA',1,'DA',Secv_Testare.CURRVAL);
INSERT INTO TESTARE_DETALII VALUES(Secv_Testare_Detalii.NEXTVAL,'Porne?te dintr-un document sau dintr-un fi?ier (dac? aplica?ia are extensii asociate)','DA',1,'DA',Secv_Testare.CURRVAL);
INSERT INTO TESTARE_DETALII VALUES(Secv_Testare_Detalii.NEXTVAL,'Porne?te de la bara Lansare rapid?','DA',1,'DA',Secv_Testare.CURRVAL);
INSERT INTO TESTARE_DETALII VALUES(Secv_Testare_Detalii.NEXTVAL,'Finalizeaz? o instalare minim?','DA',1,'DA',Secv_Testare.CURRVAL);

INSERT INTO CONTRACTE VALUES(Secv_Contracte.NEXTVAL,TO_DATE(SYSDATE),700,0,1800,0,TO_DATE(SYSDATE+70),TO_DATE(SYSDATE+90),0,3);

INSERT INTO FACTURI(Serie_Numar,Total_cu_Tva,Data_Scadenta,Nr_Contract,Tip_Factura) Values(TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.NEXTVAL,700,sysdate,Secv_Contracte.CURRVAL,'AVANS');
INSERT INTO FACTURI_Detalii(Cod_Detalii,Nr_Crt,Cantitate_Ore,Serie_Numar,Denumire,Pretul_Unitar) VALUES(Secv_Facturi_Detalii.NEXTVAL,1,1,TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.CURRVAL,'AVANS',700);
INSERT INTO FACTURI(Serie_Numar,Total_cu_Tva,Data_Scadenta,Nr_Contract) Values(TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.NEXTVAL,1100,sysdate,Secv_Contracte.CURRVAL);
INSERT INTO FACTURI_Detalii(Cod_Detalii,Nr_Crt,Cantitate_Ore,Serie_Numar,Id_Serviciu) VALUES(Secv_Facturi_Detalii.NEXTVAL,1,45,TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.CURRVAL,3);
INSERT INTO FACTURI_Detalii(Cod_Detalii,Nr_Crt,Cantitate_Ore,Serie_Numar,Denumire,Pretul_Unitar) VALUES(Secv_Facturi_Detalii.NEXTVAL,2,1,TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.CURRVAL,'AVANS',-700);

INSERT INTO ACHIZITII Values(TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Achizitii.NEXTVAL,sysdate,720,Secv_Contracte.CURRVAL);
INSERT INTO ACHIZITII_DETALII VALUES(Secv_Achizitii_Detalii.NEXTVAL,'Calculatoare','buc',30,24,720,TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Achizitii.CURRVAL);

INSERT INTO CONTRACTE VALUES(Secv_Contracte.NEXTVAL,TO_DATE(SYSDATE),300,0,600,0,TO_DATE(SYSDATE+70),TO_DATE(SYSDATE+90),0,3);

INSERT INTO FACTURI(Serie_Numar,Total_cu_Tva,Data_Scadenta,Nr_Contract,Tip_Factura) Values(TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.NEXTVAL,300,sysdate,Secv_Contracte.CURRVAL,'AVANS');
INSERT INTO FACTURI_Detalii(Cod_Detalii,Nr_Crt,Cantitate_Ore,Serie_Numar,Denumire,Pretul_Unitar) VALUES(Secv_Facturi_Detalii.NEXTVAL,1,1,TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.CURRVAL,'AVANS',300);
INSERT INTO FACTURI(Serie_Numar,Total_cu_Tva,Data_Scadenta,Nr_Contract) Values(TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.NEXTVAL,300,sysdate,Secv_Contracte.CURRVAL);
INSERT INTO FACTURI_Detalii(Cod_Detalii,Nr_Crt,Cantitate_Ore,Serie_Numar,Id_Serviciu) VALUES(Secv_Facturi_Detalii.NEXTVAL,1,15,TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.CURRVAL,3);
INSERT INTO FACTURI_Detalii(Cod_Detalii,Nr_Crt,Cantitate_Ore,Serie_Numar,Denumire,Pretul_Unitar) VALUES(Secv_Facturi_Detalii.NEXTVAL,2,1,TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.CURRVAL,'AVANS',-300);

INSERT INTO TESTARE VALUES(Secv_Testare.NEXTVAL,sysdate,Secv_Contracte.CURRVAL);
INSERT INTO TESTARE_DETALII VALUES(Secv_Testare_Detalii.NEXTVAL,'Completeaz? o instalare custom','DA',1,'DA',Secv_Testare.CURRVAL);
INSERT INTO TESTARE_DETALII VALUES(Secv_Testare_Detalii.NEXTVAL,'Completeaz? o instalare complet?','DA',1,'DA',Secv_Testare.CURRVAL);
INSERT INTO TESTARE_DETALII VALUES(Secv_Testare_Detalii.NEXTVAL,'Functioneaza corect la nivel de re?ea','DA',1,'DA',Secv_Testare.CURRVAL);
INSERT INTO TESTARE_DETALII VALUES(Secv_Testare_Detalii.NEXTVAL,'Finalizeaz? o instalare local? atunci c�nd utiliza?i Ad?ugare sau eliminare programe','DA',1,'DA',Secv_Testare.CURRVAL);
INSERT INTO TESTARE_DETALII VALUES(Secv_Testare_Detalii.NEXTVAL,'Finalizeaz? o instalare minim?','DA',1,'DA',Secv_Testare.CURRVAL);

INSERT INTO ACHIZITII Values(TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Achizitii.NEXTVAL,sysdate,720,Secv_Contracte.CURRVAL);
INSERT INTO ACHIZITII_DETALII VALUES(Secv_Achizitii_Detalii.NEXTVAL,'Calculatoare','buc',30,24,720,TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Achizitii.CURRVAL);

INSERT INTO CONTRACTE VALUES(Secv_Contracte.NEXTVAL,TO_DATE(SYSDATE),500,0,1500,0,TO_DATE(SYSDATE+70),TO_DATE(SYSDATE+90),0,4);

INSERT INTO FACTURI(Serie_Numar,Total_cu_Tva,Data_Scadenta,Nr_Contract,Tip_Factura) Values(TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.NEXTVAL,500,sysdate,Secv_Contracte.CURRVAL,'AVANS');
INSERT INTO FACTURI_Detalii(Cod_Detalii,Nr_Crt,Cantitate_Ore,Serie_Numar,Denumire,Pretul_Unitar) VALUES(Secv_Facturi_Detalii.NEXTVAL,1,1,TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.CURRVAL,'AVANS',500);
INSERT INTO FACTURI(Serie_Numar,Total_cu_Tva,Data_Scadenta,Nr_Contract) Values(TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.NEXTVAL,1000,sysdate,Secv_Contracte.CURRVAL);
INSERT INTO FACTURI_Detalii(Cod_Detalii,Nr_Crt,Cantitate_Ore,Serie_Numar,Id_Serviciu) VALUES(Secv_Facturi_Detalii.NEXTVAL,1,30,TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.CURRVAL,2);
INSERT INTO FACTURI_Detalii(Cod_Detalii,Nr_Crt,Cantitate_Ore,Serie_Numar,Denumire,Pretul_Unitar) VALUES(Secv_Facturi_Detalii.NEXTVAL,2,1,TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.CURRVAL,'AVANS',-500);

INSERT INTO CONTRACTE VALUES(Secv_Contracte.NEXTVAL,TO_DATE(SYSDATE),200,0,720,0,TO_DATE(SYSDATE+70),TO_DATE(SYSDATE+90),0,5);

INSERT INTO FACTURI(Serie_Numar,Total_cu_Tva,Data_Scadenta,Nr_Contract,Tip_Factura) Values(TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.NEXTVAL,200,sysdate,Secv_Contracte.CURRVAL,'AVANS');
INSERT INTO FACTURI_Detalii(Cod_Detalii,Nr_Crt,Cantitate_Ore,Serie_Numar,Denumire,Pretul_Unitar) VALUES(Secv_Facturi_Detalii.NEXTVAL,1,1,TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.CURRVAL,'AVANS',200);
INSERT INTO FACTURI(Serie_Numar,Total_cu_Tva,Data_Scadenta,Nr_Contract) Values(TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.NEXTVAL,200,sysdate,Secv_Contracte.CURRVAL);
INSERT INTO FACTURI_Detalii(Cod_Detalii,Nr_Crt,Cantitate_Ore,Serie_Numar,Id_Serviciu) VALUES(Secv_Facturi_Detalii.NEXTVAL,1,8,TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.CURRVAL,1);
INSERT INTO FACTURI_Detalii(Cod_Detalii,Nr_Crt,Cantitate_Ore,Serie_Numar,Denumire,Pretul_Unitar) VALUES(Secv_Facturi_Detalii.NEXTVAL,2,1,TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.CURRVAL,'AVANS',-200);

INSERT INTO CONTRACTE VALUES(Secv_Contracte.NEXTVAL,TO_DATE(SYSDATE),200,0,720,0,TO_DATE(SYSDATE+70),TO_DATE(SYSDATE+90),0,5);

INSERT INTO FACTURI(Serie_Numar,Total_cu_Tva,Data_Scadenta,Nr_Contract,Tip_Factura) Values(TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.NEXTVAL,200,sysdate,Secv_Contracte.CURRVAL,'AVANS');
INSERT INTO FACTURI_Detalii(Cod_Detalii,Nr_Crt,Cantitate_Ore,Serie_Numar,Denumire,Pretul_Unitar) VALUES(Secv_Facturi_Detalii.NEXTVAL,1,1,TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.CURRVAL,'AVANS',200);
INSERT INTO FACTURI(Serie_Numar,Total_cu_Tva,Data_Scadenta,Nr_Contract) Values(TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.NEXTVAL,200,sysdate,Secv_Contracte.CURRVAL);
INSERT INTO FACTURI_Detalii(Cod_Detalii,Nr_Crt,Cantitate_Ore,Serie_Numar,Id_Serviciu) VALUES(Secv_Facturi_Detalii.NEXTVAL,1,8,TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.CURRVAL,1);
INSERT INTO FACTURI_Detalii(Cod_Detalii,Nr_Crt,Cantitate_Ore,Serie_Numar,Denumire,Pretul_Unitar) VALUES(Secv_Facturi_Detalii.NEXTVAL,2,1,TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.CURRVAL,'AVANS',-200);

INSERT INTO CONTRACTE VALUES(Secv_Contracte.NEXTVAL,TO_DATE(SYSDATE),500,0,1500,0,TO_DATE(SYSDATE+70),TO_DATE(SYSDATE+90),0,1);

INSERT INTO FACTURI(Serie_Numar,Total_cu_Tva,Data_Scadenta,Nr_Contract,Tip_Factura) Values(TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.NEXTVAL,500,sysdate,Secv_Contracte.CURRVAL,'AVANS');
INSERT INTO FACTURI_Detalii(Cod_Detalii,Nr_Crt,Cantitate_Ore,Serie_Numar,Denumire,Pretul_Unitar) VALUES(Secv_Facturi_Detalii.NEXTVAL,1,1,TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.CURRVAL,'AVANS',500);
INSERT INTO FACTURI(Serie_Numar,Total_cu_Tva,Data_Scadenta,Nr_Contract) Values(TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.NEXTVAL,1000,sysdate,Secv_Contracte.CURRVAL);
INSERT INTO FACTURI_Detalii(Cod_Detalii,Nr_Crt,Cantitate_Ore,Serie_Numar,Id_Serviciu) VALUES(Secv_Facturi_Detalii.NEXTVAL,1,30,TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.CURRVAL,2);
INSERT INTO FACTURI_Detalii(Cod_Detalii,Nr_Crt,Cantitate_Ore,Serie_Numar,Denumire,Pretul_Unitar) VALUES(Secv_Facturi_Detalii.NEXTVAL,2,1,TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.CURRVAL,'AVANS',-500);

INSERT INTO CONTRACTE VALUES(Secv_Contracte.NEXTVAL,TO_DATE(SYSDATE),150,0,810,0,TO_DATE(SYSDATE+70),TO_DATE(SYSDATE+90),0,6);

INSERT INTO FACTURI(Serie_Numar,Total_cu_Tva,Data_Scadenta,Nr_Contract,Tip_Factura) Values(TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.NEXTVAL,150,sysdate,Secv_Contracte.CURRVAL,'AVANS');
INSERT INTO FACTURI_Detalii(Cod_Detalii,Nr_Crt,Cantitate_Ore,Serie_Numar,Denumire,Pretul_Unitar) VALUES(Secv_Facturi_Detalii.NEXTVAL,1,1,TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.CURRVAL,'AVANS',150);
INSERT INTO FACTURI(Serie_Numar,Total_cu_Tva,Data_Scadenta,Nr_Contract) Values(TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.NEXTVAL,660,sysdate,Secv_Contracte.CURRVAL);
INSERT INTO FACTURI_Detalii(Cod_Detalii,Nr_Crt,Cantitate_Ore,Serie_Numar,Id_Serviciu) VALUES(Secv_Facturi_Detalii.NEXTVAL,1,9,TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.CURRVAL,1);
INSERT INTO FACTURI_Detalii(Cod_Detalii,Nr_Crt,Cantitate_Ore,Serie_Numar,Denumire,Pretul_Unitar) VALUES(Secv_Facturi_Detalii.NEXTVAL,2,1,TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.CURRVAL,'AVANS',-150);

INSERT INTO CONTRACTE VALUES(Secv_Contracte.NEXTVAL,TO_DATE(SYSDATE),200,0,720,0,TO_DATE(SYSDATE+70),TO_DATE(SYSDATE+90),0,1);

INSERT INTO FACTURI(Serie_Numar,Total_cu_Tva,Data_Scadenta,Nr_Contract,Tip_Factura) Values(TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.NEXTVAL,200,sysdate,Secv_Contracte.CURRVAL,'AVANS');
INSERT INTO FACTURI_Detalii(Cod_Detalii,Nr_Crt,Cantitate_Ore,Serie_Numar,Denumire,Pretul_Unitar) VALUES(Secv_Facturi_Detalii.NEXTVAL,1,1,TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.CURRVAL,'AVANS',200);
INSERT INTO FACTURI(Serie_Numar,Total_cu_Tva,Data_Scadenta,Nr_Contract) Values(TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.NEXTVAL,520,sysdate,Secv_Contracte.CURRVAL);
INSERT INTO FACTURI_Detalii(Cod_Detalii,Nr_Crt,Cantitate_Ore,Serie_Numar,Id_Serviciu) VALUES(Secv_Facturi_Detalii.NEXTVAL,1,8,TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.CURRVAL,1);
INSERT INTO FACTURI_Detalii(Cod_Detalii,Nr_Crt,Cantitate_Ore,Serie_Numar,Denumire,Pretul_Unitar) VALUES(Secv_Facturi_Detalii.NEXTVAL,2,1,TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.CURRVAL,'AVANS',-200);

INSERT INTO ACHIZITII Values(TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Achizitii.NEXTVAL,sysdate,300,Secv_Contracte.CURRVAL);
INSERT INTO ACHIZITII_DETALII VALUES(Secv_Achizitii_Detalii.NEXTVAL,'Licenta ORACLE','buc',1,300,300,TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Achizitii.CURRVAL);

INSERT INTO CONTRACTE VALUES(Secv_Contracte.NEXTVAL,TO_DATE(SYSDATE),230,0,765,0,TO_DATE(SYSDATE+70),TO_DATE(SYSDATE+90),0,14);

INSERT INTO FACTURI(Serie_Numar,Total_cu_Tva,Data_Scadenta,Nr_Contract,Tip_Factura) Values(TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.NEXTVAL,230,sysdate,Secv_Contracte.CURRVAL,'AVANS');
INSERT INTO FACTURI_Detalii(Cod_Detalii,Nr_Crt,Cantitate_Ore,Serie_Numar,Denumire,Pretul_Unitar) VALUES(Secv_Facturi_Detalii.NEXTVAL,1,1,TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.CURRVAL,'AVANS',230);
INSERT INTO FACTURI(Serie_Numar,Total_cu_Tva,Data_Scadenta,Nr_Contract) Values(TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.NEXTVAL,535,sysdate,Secv_Contracte.CURRVAL);
INSERT INTO FACTURI_Detalii(Cod_Detalii,Nr_Crt,Cantitate_Ore,Serie_Numar,Id_Serviciu) VALUES(Secv_Facturi_Detalii.NEXTVAL,1,17,TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.CURRVAL,2);
INSERT INTO FACTURI_Detalii(Cod_Detalii,Nr_Crt,Cantitate_Ore,Serie_Numar,Denumire,Pretul_Unitar) VALUES(Secv_Facturi_Detalii.NEXTVAL,2,1,TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.CURRVAL,'AVANS',-230);

INSERT INTO CONTRACTE VALUES(Secv_Contracte.NEXTVAL,TO_DATE(SYSDATE),500,0,1500,0,TO_DATE(SYSDATE+70),TO_DATE(SYSDATE+90),0,1);

INSERT INTO FACTURI(Serie_Numar,Total_cu_Tva,Data_Scadenta,Nr_Contract,Tip_Factura) Values(TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.NEXTVAL,500,sysdate,Secv_Contracte.CURRVAL,'AVANS');
INSERT INTO FACTURI_Detalii(Cod_Detalii,Nr_Crt,Cantitate_Ore,Serie_Numar,Denumire,Pretul_Unitar) VALUES(Secv_Facturi_Detalii.NEXTVAL,1,1,TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.CURRVAL,'AVANS',500);
INSERT INTO FACTURI(Serie_Numar,Total_cu_Tva,Data_Scadenta,Nr_Contract) Values(TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.NEXTVAL,1000,sysdate,Secv_Contracte.CURRVAL);
INSERT INTO FACTURI_Detalii(Cod_Detalii,Nr_Crt,Cantitate_Ore,Serie_Numar,Id_Serviciu) VALUES(Secv_Facturi_Detalii.NEXTVAL,1,30,TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.CURRVAL,2);
INSERT INTO FACTURI_Detalii(Cod_Detalii,Nr_Crt,Cantitate_Ore,Serie_Numar,Denumire,Pretul_Unitar) VALUES(Secv_Facturi_Detalii.NEXTVAL,2,1,TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.CURRVAL,'AVANS',-500);

INSERT INTO ACHIZITII Values(TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Achizitii.NEXTVAL,sysdate,720,Secv_Contracte.CURRVAL);
INSERT INTO ACHIZITII_DETALII VALUES(Secv_Achizitii_Detalii.NEXTVAL,'Placa Video','buc',30,24,720,TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Achizitii.CURRVAL);


INSERT INTO CONTRACTE VALUES(Secv_Contracte.NEXTVAL,TO_DATE(SYSDATE),1600,0,3500,0,TO_DATE(SYSDATE+70),TO_DATE(SYSDATE+90),0,1);

INSERT INTO FACTURI(Serie_Numar,Total_cu_Tva,Data_Scadenta,Nr_Contract,Tip_Factura) Values(TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.NEXTVAL,1600,sysdate,Secv_Contracte.CURRVAL,'AVANS');
INSERT INTO FACTURI_Detalii(Cod_Detalii,Nr_Crt,Cantitate_Ore,Serie_Numar,Denumire,Pretul_Unitar) VALUES(Secv_Facturi_Detalii.NEXTVAL,1,1,TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.CURRVAL,'AVANS',1600);
INSERT INTO FACTURI(Serie_Numar,Total_cu_Tva,Data_Scadenta,Nr_Contract) Values(TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.NEXTVAL,1900,sysdate,Secv_Contracte.CURRVAL);
INSERT INTO FACTURI_Detalii(Cod_Detalii,Nr_Crt,Cantitate_Ore,Serie_Numar,Id_Serviciu) VALUES(Secv_Facturi_Detalii.NEXTVAL,1,70,TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.CURRVAL,3);
INSERT INTO FACTURI_Detalii(Cod_Detalii,Nr_Crt,Cantitate_Ore,Serie_Numar,Denumire,Pretul_Unitar) VALUES(Secv_Facturi_Detalii.NEXTVAL,2,1,TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.CURRVAL,'AVANS',-1600);

INSERT INTO ACHIZITII Values(TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Achizitii.NEXTVAL,sysdate,720,Secv_Contracte.CURRVAL);
INSERT INTO ACHIZITII_DETALII VALUES(Secv_Achizitii_Detalii.NEXTVAL,'Calculatoare','buc',30,24,720,TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Achizitii.CURRVAL);


INSERT INTO CONTRACTE VALUES(Secv_Contracte.NEXTVAL,TO_DATE(SYSDATE),220,0,765,0,TO_DATE(SYSDATE+70),TO_DATE(SYSDATE+90),0,14);

INSERT INTO FACTURI(Serie_Numar,Total_cu_Tva,Data_Scadenta,Nr_Contract,Tip_Factura) Values(TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.NEXTVAL,220,sysdate,Secv_Contracte.CURRVAL,'AVANS');
INSERT INTO FACTURI_Detalii(Cod_Detalii,Nr_Crt,Cantitate_Ore,Serie_Numar,Denumire,Pretul_Unitar) VALUES(Secv_Facturi_Detalii.NEXTVAL,1,1,TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.CURRVAL,'AVANS',220);
INSERT INTO FACTURI(Serie_Numar,Total_cu_Tva,Data_Scadenta,Nr_Contract) Values(TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.NEXTVAL,545,sysdate,Secv_Contracte.CURRVAL);
INSERT INTO FACTURI_Detalii(Cod_Detalii,Nr_Crt,Cantitate_Ore,Serie_Numar,Id_Serviciu) VALUES(Secv_Facturi_Detalii.NEXTVAL,1,17,TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.CURRVAL,2);
INSERT INTO FACTURI_Detalii(Cod_Detalii,Nr_Crt,Cantitate_Ore,Serie_Numar,Denumire,Pretul_Unitar) VALUES(Secv_Facturi_Detalii.NEXTVAL,2,1,TO_CHAR((sysdate),'YYYY') ||'/' ||Secv_Facturi.CURRVAL,'AVANS',-220);


