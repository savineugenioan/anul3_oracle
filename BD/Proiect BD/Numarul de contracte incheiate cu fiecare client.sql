CLEAR SCREEN
TTITLE 'Numarul de contracte incheiate cu fiecare client'
SET FEEDBACK OFF
COLUMN Numarul_de_Contracte Heading 'NUMRUL DE CONTRACTE'
SELECT a.Cod_Client,CUI,Denumire, COUNT( b.Cod_Client) AS Numarul_de_Contracte FROM CLIENTI a INNER JOIN CONTRACTE b  ON a.Cod_Client=b.Cod_Client GROUP BY(a.Cod_Client,Denumire,CUI);