CLEAR SCREEN

ALTER TABLE Produse MODIFY(CodP varchar2(4));
DESCRIBE Produse;

ALTER TABLE ProdDoc ADD(IE number(2));
Describe ProdDoc;

ALTER TABLE Documente ADD(Valoarea number(20));
DESCRIBE Documente;

ALTER TABLE ProdDoc ADD(CONSTRAINT ck_Cant CHECK (Cant>=50));
DESCRIBE ProdDoc;

ALTER TABLE Clienti_Intermed ADD CONSTRAINT fk_CodC_Intermed FOREIGN KEY(CodC_Intermed) References Clienti(codC);
ALTER TABLE Clienti_Intermed ADD CONSTRAINT ck_den_intermed CHECK ( DenC_Intermed IN ('GOODS','FLANCO'));
ALTER TABLE Clienti_Intermed DROP CONSTRAINT ck_den_intermed;
ALTER TABLE Clienti_Intermed DROP PRIMARY KEY CASCADE ;
DESCRIBE clienti_intermed;

/*ALTER TABLE Clienti_Noi RENAME TO Clienti_Intermed;
DESCRIBE Clienti_Intermed;*/