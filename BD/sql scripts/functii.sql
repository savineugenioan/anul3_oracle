PROMPT 'Sa se selecteze din dual : e^4,4^2,log2(4),ln(4)si ln(4) rotunjit la 2 zecimale
SELECT EXP(4),POWER(4,2),LOG(2,4) LogBaza2,LN(4),ROUND(LN(4),2) Rotunjire FROM DUAL;
PRompt rotunjiri spre elementul urmator/anterior
SELECT CEIL(4.12), FLOOR(4.12) FROM DUAL;
PROMPT trunchieri
SELECT TRUNC(141.231,-1), TRUNC (141.231,-2), TRUNC (141.231,-3), TRUNC(141.231,1) FROM DUAL;
/*PROMPT afisam data nasterii
 ACCEPT data PROMPT 'Introduceti data nasterii'
SELECT TO_CHAR(To_DATE('&data','DD-MM-YYYY'),'Day') ZI_SAPT FROM DUAL;*/
PROMPT substring
SELECT SUBSTR('CLIENTI',2,4) AS EXTR FROM DUAL;
SELECT DenF, SUBSTR (Denf,2) EXTR FROM FURNIZORI;
SELECT DenP, SUBSTR(DenP,3,5) EXTR FROM Produse;
ALTER SESSION SET NLS_DATE_FORMAT='DD-MM-YYYY';
SELECT CodD,DenD,Data FROM Documente WHERE UPPER (DenD)='FACT';
SELECT INITCAP(DenC) LiteraMareNume,INITCAP(Loc) FROM CLIENTI ;
SELECT 'PRODUSUL ' || LOWER(DenP) || ' Are Pretul Unitar:' ||Pret|| 'LEI. STOCUL actual este:' ||Stoc|| ' DE ' ||Um From PRODUSE;
SELECT DenF Furnizor ,CONCAT(Adr,Loc) "Cu sediul in:" FROM Furnizori WHERE DenF = 'GOODS';
/*ACCEPT Cod PROMPT 'Introduceti cod document';
SELECT 'DOCUMENTUL: ' || LOWER(DenD)|| ' are codul' || CodD || ' si DATA Tranzactiei ' || data || '.' FROM DOCUMENTE WHERE CODD=&Cod;*/

PROMPT Asezare in pagina cu LPAD si RPAD
SELECT LPAD(Codf,20,'*'),LPAD(Denf,20), LPAD(Loc,20,'-') FROM Furnizori;
SELECT RPAD(Codf,20,'*'),RPAD(Denf,20), RPAD(Loc,20,'-') FROM Furnizori;

SELECT Codf,Denf,Loc FROM Furnizori WHERE Loc NOT LIKE 'B%';
SELECT Codf,Denf,Loc FROM Furnizori WHERE UPPER(Loc) <> 'BUCURESTI';
PROMPT Clintii a caror denumire incepe cu 'F'
SELECT CodC,DenC FROM Clienti WHERE UPPER(SUBSTR(DenC,1,1))='F';
SELECT DenC, Loc, LENGTH(Loc) Lungime_Doc FROM Clienti;
SELECT LENGTH('LOCALITATE') Lungime, Replace('LOCALITATE','T','*') Fara_T FROM DUAL;
SELECT  Loc LOCALIT, LENGTH(Loc) L_Initiala,TRANSLATE(Loc,'RA','r') sir_dupa_stergere, LENGTH(TRANSLATE(loc,'RA','r')) lungime_sir_ramas FROM Clienti;