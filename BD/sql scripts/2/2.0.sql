CLEAR SCREEN;
Prompt Stergere tabele Cititori si legitimatii definite anterior
DROP TABLE Cititori CASCADE CONSTRAINTS;
DROP TABLE Legitimatii CASCADE CONSTRAINTS;
PROMPT Creare TABELA Cititori
CREATE TABLE Cititori(
CodCititor NUMBER(4),
NumeCititor VARCHAR2(20),
CNP NUMBER(13),
LocCititor VARCHAR2(20),
Str_nrCititor VARCHAR2(10),
TelefCititor NUMBER(10),
EmailCititor VARCHAR2(50),
CONSTRAINT pk_codcititor PRIMARY KEY(CodCititor)
);
PROMPT Afisare structura tabela Cititori
DESCRIBE Cititori;
CREATE TABLE Legitimatii(
NrLegitimatie NUMBER(3),
SerieLegitimatie VARCHAR2(10),
DataLegitimatie DATE,
Valabilitate NUMBER(3),
CodCititor NUMBER(4),
CONSTRAINT pk_nrlegitimatie PRIMARY KEY(NrLegitimatie),
CONSTRAINT fk_CodCititor Foreign Key(CodCititor) REFERENCES Cititori(CodCititor)
);
PROMPT Afisare structura tabela Legitimatii
DESCRIBE Legitimatii;