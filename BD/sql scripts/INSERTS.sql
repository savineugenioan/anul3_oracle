CLEAR SCREEN

DELETE FROM CLIENTI;
DELETE FROM FURNIZORI;
DELETE FROM PRODUSE;
DELETE FROM TRANZACTII;
DELETE FROM DOCUMENTE;
DELETE FROM PRODDOC;
CLEAR BUFFER;
CLEAR COLUMN;


INSERT INTO Clienti(CodC, DenC, Adr, Loc, Cont, Banca) Values('0','GOODS','PIPERA 135', 'BUCURESTI','A1234567890', 'BRD');
INSERT INTO Clienti Values('1','GOODS','PIPERA 135', 'BUCURESTI','A1234567890', 'BRD');
INSERT INTO Clienti(CodC, DenC, Adr, Loc, Cont, Banca) Values('2','Depozit PC','Stefan cel mare 110', 'BUCURESTI','A1234556780', 'BCR');
INSERT INTO Clienti(CodC, DenC, Adr, Loc, Cont, Banca) Values('3','Flamingo','mihai Eminescu 18', 'Cluj','A1231231235', 'BRD');
INSERT INTO Clienti(CodC, DenC, Adr, Loc, Cont, Banca) Values('4','Ultra Pro','Mihai Bravu 11', 'TIMISOARA','B1234567890', 'BRD');
INSERT INTO Clienti(CodC, DenC, Adr, Loc, Cont, Banca) Values('5','FLANCO','DOROBANTILOR 130', 'CLUJ','C1234567890', 'BCR');
SELECT * FROM Clienti;

INSERT INTO Furnizori Values('1','GOODS','PIPERA 135', 'BUCURESTI','A1234567890', 'BRD');
INSERT INTO Furnizori Values('2','ComputerNT','Gral Popescu 13', 'Iasi','A1234123412', 'BRD');
INSERT INTO Furnizori Values('3','Pyton','Charles de Gaule 117', 'Cluj','A1234512345', 'BCR');
INSERT INTO Furnizori Values('4','Blue Ridge','Magheru 307', 'BUCURESTI','B1234554321', 'BRD');
INSERT INTO Furnizori Values('5','Deck Electronik','Lacul Alb 35', 'Iasi','B1234567777', 'BCR');
SELECT * FROM Furnizori;

INSERT INTO Produse Values('P1','MONITOR 17 INCH','BUC','3500000','1000', TO_DATE('01/08/2006','DD/MM/YYYY'));
INSERT INTO Produse Values('P2','CD-RW ASUS 24X10X40X','BUC','1000000','500', TO_DATE('01/08/2005','DD/MM/YYYY'));
INSERT INTO Produse Values('P3','TASTATURA QWERTY','BUC','300000','100', TO_DATE('01/06/2004','DD/MM/YYYY'));
INSERT INTO Produse Values('P4','CPU AMD ATHLON 1.4GHZ','BUC','2700000','700', TO_DATE('01/12/2004','DD/MM/YYYY'));
INSERT INTO Produse Values('P5','Mouse A4TECH','BUC','100000','150', TO_DATE('01/06/2004','DD/MM/YYYY'));
SELECT * FROM Produse;

INSERT INTO Tranzactii Values('T1','R', TO_DATE('01/08/2003 02:12:39','MM/DD/YYYY HH:MI:SS'),'3','1');
INSERT INTO Tranzactii Values('T2','R', TO_DATE('11/10/2003 10:20:09','MM/DD/YYYY HH:MI:SS'),'4','1');
INSERT INTO Tranzactii Values('T3','L', TO_DATE('12/10/2003 12:12:30','MM/DD/YYYY HH:MI:SS'),'1','5');
INSERT INTO Tranzactii Values('T4','L', TO_DATE('02/11/2003 04:55:39','MM/DD/YYYY HH:MI:SS'),'1','2');
INSERT INTO Tranzactii Values('T5','R', TO_DATE('01/11/2003 02:12:39','MM/DD/YYYY HH:MI:SS'),'3','1');
SELECT * FROM Tranzactii;

INSERT INTO Documente(CodD,DenD,Data,CodT) Values('10123','FACT',TO_DATE('01/08/03 ','MM/DD/YY'),'T1');
INSERT INTO Documente(CodD,DenD,Data,CodT) Values('20123','NIR',TO_DATE('01/08/03 ','MM/DD/YY'),'T1');
INSERT INTO Documente(CodD,DenD,Data,CodT) Values('10124','FACT',TO_DATE('11/10/03 ','MM/DD/YY'),'T2');
INSERT INTO Documente(CodD,DenD,Data,CodT) Values('20124','Nir',TO_DATE('11/10/03 ','MM/DD/YY'),'T2');
INSERT INTO Documente(CodD,DenD,Data,CodT) Values('30122','Aviz',TO_DATE('12/10/03 ','MM/DD/YY'),'T3');
INSERT INTO Documente(CodD,DenD,Data,CodT) Values('10125','FACT',TO_DATE('12/10/03 ','MM/DD/YY'),'T3');
INSERT INTO Documente(CodD,DenD,Data,CodT) Values('30123','FACT',TO_DATE('02/11/03 ','MM/DD/YY'),'T4');
INSERT INTO Documente(CodD,DenD,Data,CodT) Values('10126','FACT',TO_DATE('02/11/03 ','MM/DD/YY'),'T4');
INSERT INTO Documente(CodD,DenD,Data,CodT) Values('40123','CHIT',TO_DATE('02/11/03 ','MM/DD/YY'),'T4');
SELECT * FROM Documente;

INSERT INTO ProdDoc Values('10123','P1','buc',500,null);
INSERT INTO ProdDoc Values('10123','P2','buc',500,null);
INSERT INTO ProdDoc Values('20123','P1','buc',500,null);
INSERT INTO ProdDoc Values('20123','P2','buc',500,null);
INSERT INTO ProdDoc Values('10124','P3','buc',100,null);
INSERT INTO ProdDoc Values('10124','P4','buc',500,null);
INSERT INTO ProdDoc Values('10124','P5','buc',100,null);
INSERT INTO ProdDoc Values('20124','P3','buc',100,null);
INSERT INTO ProdDoc Values('20124','P4','buc',450,null);
INSERT INTO ProdDoc Values('20124','P5','buc',100,null);
INSERT INTO ProdDoc Values('30122','P1','buc',100,null);
INSERT INTO ProdDoc Values('30122','P2','buc',200,null);
INSERT INTO ProdDoc Values('10125','P1','buc',100,null);
INSERT INTO ProdDoc Values('10125','P2','buc',200,null);
INSERT INTO ProdDoc Values('30123','P1','buc',300,null);
INSERT INTO ProdDoc Values('30123','P4','buc',500,null);
INSERT INTO ProdDoc Values('10126','P1','buc',300,null);
INSERT INTO ProdDoc Values('10126','P4','buc',500,null);
SELECT * FROM ProdDoc;